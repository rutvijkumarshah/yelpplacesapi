package com.example.yelpplaces.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.yelpplaces.NetworkResult
import com.example.yelpplaces.buildRepository
import com.yelp.fusion.client.models.Business
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

const val TAG = "MainViewModel"

class MainViewModel : ViewModel() {

    val repository = buildRepository()

    private val businessLiveData = MutableLiveData<ArrayList<Business>>()

    val businesslist = businessLiveData as LiveData<ArrayList<Business>>

    fun search() {
        viewModelScope.launch {
            withContext(Dispatchers.IO) {
                val result = repository.searchBusinessLocations("American", "San Francisco", 20)
                when (result) {
                    is NetworkResult.Success -> {
                        businessLiveData.postValue(result.data)
                    }
                    is NetworkResult.Error -> {
                        Log.e(TAG, "search:Error code=${result.errorCode}", result.exception)
                    }
                }
            }

        }
    }

}