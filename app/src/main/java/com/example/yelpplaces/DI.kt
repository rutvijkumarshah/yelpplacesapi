package com.example.yelpplaces

import com.yelp.fusion.client.connection.YelpFusionApi
import com.yelp.fusion.client.connection.YelpFusionApiFactory

const val API_KEY = "PbrZ1zy5yQQx6uJFtFzl8rOcdO3IUv4uMgtG3sIw6nU-AA6k4ED65wed63Oarp9J8dj1EnBhAcSslpra7siyC2Rqa-Qbwq1gWMAQlqnMcDw2aXrezz1ZnmfbZKKIW3Yx"

fun provideYelpFusionFactory(): YelpFusionApiFactory {
    return YelpFusionApiFactory()
}

fun provideYelpFusionApi(factory: YelpFusionApiFactory): YelpFusionApi {
    return factory.createAPI(API_KEY)
}

fun provideYelpFusionRepository(api: YelpFusionApi): YelpFusionRepository {
    return YelpFusionRepository(api)
}

fun buildRepository(): YelpFusionRepository {
    val factory = provideYelpFusionFactory()
    val api = provideYelpFusionApi(factory)
    val repository = provideYelpFusionRepository(api)
    return repository
}