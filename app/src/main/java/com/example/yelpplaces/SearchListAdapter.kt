package com.example.yelpplaces

import android.content.Context
import android.util.DisplayMetrics
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import com.yelp.fusion.client.models.Business


class SearchItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {

    fun onBind(business: Business) {
        val imageView: ImageView = view.findViewById(R.id.businessImage)
        val textView: TextView = view.findViewById(R.id.businessName)

        textView.text = business.name
        val totalHeight = getWindowHeight(textView.context)
        imageView.layoutParams.height = totalHeight / 5
        imageView.requestLayout()
        Picasso.get().load(business.imageUrl)
                .into(imageView)

    }
}

class SearchListAdapter(private val data: MutableList<Business>) : RecyclerView.Adapter<SearchItemViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchItemViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.search_item, parent, false)
        return SearchItemViewHolder(view)
    }

    override fun onBindViewHolder(holder: SearchItemViewHolder, position: Int) {
        val business = data[position]
        holder.onBind(business)
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun udpateData(newData: ArrayList<Business>) {
        data.addAll(newData)
        notifyDataSetChanged()
    }

}


fun getWindowHeight(context: Context): Int {
    val displayMetrics = DisplayMetrics()
    val windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager
    windowManager.defaultDisplay.getMetrics(displayMetrics)
    return displayMetrics.heightPixels
}

