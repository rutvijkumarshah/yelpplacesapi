package com.example.yelpplaces

import com.yelp.fusion.client.connection.YelpFusionApi
import com.yelp.fusion.client.models.Business
import com.yelp.fusion.client.models.SearchResponse
import retrofit2.Response
import java.io.IOException
import java.util.*

class YelpFusionRepository(private val api: YelpFusionApi) {

    suspend fun searchBusinessLocations(searchTerm: String, location: String, results: Int = 20): NetworkResult<ArrayList<Business>> {
        val searchRequest = mapOf(
                "term" to searchTerm,
                "location" to location,
                "limit" to results.toString()
        )
        val call = api.getBusinessSearch(searchRequest)
        var response: Response<SearchResponse>? = null
        var exception: java.lang.Exception? = null
        try {
            response = call.execute()
        } catch (io: IOException) {
            exception = io
        }

        val result = if (response != null) {
            if (response.isSuccessful) {
                if (response.body() != null) {
                    NetworkResult.Success(response.body().businesses)
                } else {
                    NetworkResult.Error(response.code(), IOException("Empty body receieved"))
                }
            } else {
                NetworkResult.Error(response.code())
            }
        } else {
            NetworkResult.Error(exception = exception)
        }
        return result
    }
}

sealed class NetworkResult<out T : Any> {
    data class Success<out T : Any>(val data: T) : NetworkResult<T>()
    data class Error(val errorCode: Int = -1, val exception: Exception? = null) : NetworkResult<Nothing>()
}