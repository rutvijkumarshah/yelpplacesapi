package com.example.yelpplaces

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.yelpplaces.ui.main.MainFragment


/***
 *
 *
 * applicationId ""
Yelp's API documentation is here: https://github.com/ranga543/yelp-fusion-android

1. First Query Yelp's Search API with the following:
i. Search term "American"
ii. Search limit of 20 results
iii. Search location of "San Francisco"
2. Next, display the results from the Yelp Search API in a List.
I. You must render the business image.
ii. You must show the business name.
iii. You must allow scrolling through all the results.

You may use any components or Libraries you want.
Xiaochen Huang11:34 AM
PbrZ1zy5yQQx6uJFtFzl8rOcdO3IUv4uMgtG3sIw6nU-AA6k4ED65wed63Oarp9J8dj1EnBhAcSslpra7siyC2Rqa-Qbwq1gWMAQlqnMcDw2aXrezz1ZnmfbZKKIW3Yx


 */


class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow()
        }
    }
}